(defun c:ChangeTextStyleInFence (/ ss i ent entType attribs attrib)
  ; Set the desired text style name
  (setq textStyle "V")

  ; Get selection set for all currently selected entities
  (setq ss (ssget "_I"))

  ; Check if selection set is not nil
  (if ss
    (progn
      (princ "\nSelection valid. Starting...")
      ; Loop through all entities in the selection set
      (setq i 0)
      (repeat (sslength ss)
        (setq ent (ssname ss i))
        (setq entType (cdr (assoc 0 (entget ent))))

        ; Check if the entity is a block reference (INSERT)
        (if (= entType "INSERT")
          (progn
            ; Get the block reference's attributes
            (setq attribs (entnext ent))
            (while attribs
              (setq attrib (entget attribs))
              ; Check if the entity is an attribute (ATTRIB)
              (if (= (cdr (assoc 0 attrib)) "ATTRIB")
                (progn
                  ; Change the text style of the attribute entity
                  (entmod (subst (cons 7 textStyle) (assoc 7 attrib) attrib))
                  (entupd attribs)
                )
              )
              (setq attribs (entnext attribs))
            )
          )
        )
        (setq i (+ i 1))
      )
      (princ "\nText style changed for selected entities within blocks.")
    )
    (princ "\nNo selection made or invalid selection.")
  )
  (princ)
)

; Make the command available in AutoCAD
(princ "\nType 'ChangeTextStyleInFence' to change text style within a fenced area and within blocks.")
(princ)
